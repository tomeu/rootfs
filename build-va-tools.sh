#!/bin/bash

set -ex

git config --global user.email "fdo@example.com"
git config --global user.name "freedesktop.org CI"
git clone \
    https://github.com/intel/libva-utils.git \
    -b 2.13.0 \
    --depth 1 \
    /va-utils

pushd /va-utils
meson build -D tests=true  -Dprefix=/va $EXTRA_MESON_ARGS
ninja -C build install
popd
rm -rf /va-utils
