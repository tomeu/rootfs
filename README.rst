This repo holds scripts to build kernels and rootfs archives for use in other project's CI (Mesa, Kernel, IGT, ...).

Upreving the kernel
===================

Occasionally, projects using these rootfs needs a kernel update to enable new features, device
drivers, bug fixes etc in CI jobs.

Kernel uprevs are relatively simple, but prone to lots of side-effects since
many devices from different platforms can be involved in the pipelines.

Kernel repository
-----------------

The Linux Kernel used is stored at the following repository:
https://gitlab.freedesktop.org/gfx-ci/linux

It is common for projects to need some patches on top of the version currently used. These patches might be dropped when upreving, or need to be carried on and rebased on top of new versions.

Each uprev needs to be tagged according to the standard naming: `vX.YZ-for-fdo-ci-<commit_short_SHA>`, which
can be created via the command:

:code:`git tag vX.YZ-for-fdo-ci-$(git rev-parse --short HEAD)`

Building new kernels
--------------------

When a new rootfs image is created, the Linux Kernel is built based on
the script located at `build-kernel.sh`.

Updating Kconfigs
^^^^^^^^^^^^^^^^^

When a Kernel uprev happens, it can be worth compiling and cross-compiling the
Kernel locally, in order to update the Kconfigs accordingly.  Remember that the
resulting Kconfig is a merge between the *Kconfig* in this repository and a given *defconfig*, made via `merge_config.sh` script located in the Linux Kernel sources.

Kconfigs location
"""""""""""""""""

+------------+---------------------------+-------------------------------------+
| Platform   | Mesa CI Kconfig location  | Linux tree defconfig                |
+============+===========================+=====================================+
| arm        | arm.config                | arch/arm/configs/multi_v7_defconfig |
+------------+---------------------------+-------------------------------------+
| arm64      | arm64.config              | arch/arm64/configs/defconfig        |
+------------+---------------------------+-------------------------------------+
| x86-64     | x86_64.config             | arch/x86/configs/x86_64_defconfig   |
+------------+---------------------------+-------------------------------------+

Updating image tags
-------------------

Every kernel uprev should update 3 image tags, located at two files.

:code:`.gitlab-ci.yml` tags
^^^^^^^^^^^^^^^^^^^^^^^^^^
- **KERNEL_URL** for the location of the new kernel
- **KERNEL_ROOTFS_TAG** to rebuild rootfs with the new kernel

Development routine
-------------------

1. Compile the newer kernel locally for each platform.
2. Compile device trees for ARM platforms
3. Update Kconfigs. Are new Kconfigs necessary? Is CONFIG_XYZ_BLA deprecated? Does the `merge_config.sh` override an important config?
4. Push a new development branch to `Kernel repository`_ based on the latest kernel tag used in Gitlab CI
5. Hack `build-kernel.sh` script to clone kernel from your development branch
6. Update image tags. See `Updating image tags`_
7. Run a whole pipeline in this repo
8. Update a personal branch of your project (Mesa, IGT, etc) to use the new kernel and rootfs combination, run the entire CI pipeline and make sure that all the automatic jobs should pass. If some job fails or takes too long, you will need to investigate it and probably ask for help.

When the Kernel uprev is stable
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. Push a new tag to the `Kernel repository`_
2. Update tags in this repo again
3. Open a merge request, if it is not opened yet
